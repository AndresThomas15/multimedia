from tkinter import Canvas, ROUND,BOTH,Tk

class main:
    def __init__(self,master):
        self.master = master
        self.color_fg = 'black'
        self.color_bg = 'white'
        self.old_x = None
        self.old_y = None
        self.penwidth = 5
        self.drawWidgets()
        #click izquierdo
        self.c.bind('<Button-1>',self.saveDir)
        self.c.bind('<Button-3>',self.reset)
        
        #obtener coordenadas y enviarlas a paint
    def saveDir(self,e):
        print("************")
        print(e.x)
        print("------------")
        print(e.y)
        print("************")
        
        self.paint(e,e.x,e.y)

        
        #modificar y usar punto pendiente y un ciclo :v


#Nota el programa funciona pero creo haberle entendido mal por lo que dejo una breve explicacion

#Profesor he investigado y no he podido realizar la tarea como entendí que la queria
# no he encontrado la manera de dibujar un solo punto, sin que no sea una grafica (matplotlib)
# la libreria turtle al igual que canvas solo permite dibujar lineas, pero si hubiera una forma creo
# que seria así:
#
#   1. Crear un metodo que guarde la coordenada donde el usuario haga click (saveDir lo hace)
#   2. Dibujar en pantalla el punto donde el usario ha hecho click
#   3. Usar una estructura de datos conveniente, podria ser una pila, donde se guardan las coordenadas y 
#      recordemos que una linea debe tener 2 puntos.
#   4. Cuando el usuario hace click nuevamente volveremos a realizar del punto numero 1 al 2
#   5. Realizado el punto 4 para dibujar la linea usaremos las coordenadas que estan dentro de la pila
#      para utilizar la formula de punto pendiente para saber como trazar la linea entre los 2 puntos
#   6. Utilizando un ciclo de repeticion (while o for) podemos dibujar cada uno de los puntos correctos 
#      para dibujar la linea
#
#   Estos pasos se repetirian las veces necesarias
#





    def paint(self,e, __new_X,__new_Y ):
        if self.old_x and self.old_y:
            self.c.create_line(self.old_x,self.old_y,__new_X,__new_Y,width=self.penwidth,fill=self.color_fg,capstyle=ROUND,smooth=True)

        self.old_x = e.x
        self.old_y = e.y
    

    def reset(self,e):
        self.c.destroy()
        self.drawWidgets()

       
    def drawWidgets(self):
        
        self.c = Canvas(self.master,width=500,height=400,bg=self.color_bg,)
        self.c.pack(fill=BOTH,expand=True)

        
    
if __name__ == '__main__':
    root = Tk()
    main(root)
    root.title('Drawner')
    root.mainloop()