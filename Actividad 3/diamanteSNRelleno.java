
import java.util.Scanner;

public class diamanteSNRelleno{

    static Scanner sc = new Scanner(System.in);
    static char[][] matrix;
    
    public static void main(String[]args){
        int espacios = 0;
        char punto = 'x';
        boolean flag = true;
        while(flag){
            flag = size();
        }
    }
    //Crear la matriz
    public static boolean size(){
        int n = 1;
        boolean flag = true;
        while(flag) {
            try{
                System.out.print("Ingrese un numero impar:");
                n = sc.nextInt();
                if( n % 2 != 0){
                    matrix = new char[n][n];
                    flag = false;
                    drawDiamond(n);
                    return true;
                }else{
                    System.out.println("Ingrese un numero impar por favor");
                }                    
            } catch(Exception e){
                System.out.println("Ingrese un numero por favor");
                sc.nextLine();
            }
            if( n == 0)
                return false;
        }
        return true;
    }

    public static void drawDiamond(int n){
        System.out.println("**************Dibujando**************");
        int space = (int)Math.round(n/2)-1;
        space = n - 1;
        for (int j = 1; j <= n; j++){
            for (int i = 1; i <= space; i++){
                System.out.print(" ");
            }
            space--;
            for (int i = 1; i <= 2 * j - 1; i++) {
                System.out.print("*");                
            }
            System.out.println("");
        }
        space = 1;
        for (int j = 1; j <= n - 1; j++){
            for (int i = 1; i <= space; i++){
                System.out.print(" ");
            }
            space++;
            for (int i = 1; i <= 2 * (n - j) - 1; i++){
                System.out.print("*");
            }
            System.out.println("");
        }

    }


}