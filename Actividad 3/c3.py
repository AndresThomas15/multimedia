n = int(input("Ingrese un numero impar para dibujar un rombo:"))
print("Pattern 1")
for a1 in range(1, (n+1)//2 + 1): #from row 1 to 5
    for a2 in range((n+1)//2 - a1):
        print(" ", end = "")
    for a3 in range((a1*2)-1):
        print("*",end="")
    print()

for a1 in range((n+1)//2 + 1, n + 1): #from row 6 to 9
    for a2 in range(a1 - (n+1)//2):
        print(" ", end = "")
    for a3 in range((n+1 - a1)*2 - 1):
        print("*", end = "")
    print()

h = eval(input("please enter diamond's height:"))

for i in range(1, h, 2):
    print(" "*(h//2-i//2), "*"*i,str(h//2-i//2))
for i in range(h, 0, -2):
    print(" "*(h//2-i//2), "*"*i,str(h//2-i//2))
